<h1>
    這是NodeJS練習，在練習中並將這些練習專案記錄下來。
</h1>
<h2>
    本專案練習學習參考為阮一峰大大的<a href="http://es6.ruanyifeng.com/">《ECMAScript 6 入门》</a>
</h2>
<h3>
    使用的環境：
    <table>
        <tr>
            <td align="center">
                OS
            </td>
            <td align="center">
                NodeJS
            </td>
            <td align="center">
                Npm
            </td>
            <td align="center">
                Nvm
            </td>
            <td align="center">
                IDE
            </td>
        </tr>
        <tr>
            <td align="center">
                Ubuntu 14.04 Server
            </td>
            <td align="center">
                v8.11.1
            </td>
            <td align="center">
                5.6.0
            </td>
            <td align="center">
                0.33.8
            </td>
            <td align="center">
                Visual Studio Code
            </td>
        </tr>
    </table>
</h3>
<h3>
    進度表：
</h3>
<table>
    <tr>
        <td align="center">
            <b>
                練習項目
            </b>
        </td>
        <td align="center">
            <b>
                練習內容
            </b>
        </td>
        <td align="center">
            <b>
                開始時間
            </b>
        </td>
        <td align="center">
            <b>
                結束時間
            </b>
        </td>
        <td align="center">
            <b>
                是否完成
            </b>
        </td>
        <td align="center">
            <b>
                備註
            </b>
        </td>
    </tr>
    <tr>
        <td align="center">
            <a href="./helloworld">Hello World</a>
        </td>
        <td align="center">
            印出Hello World字樣在終端機上。
        </td>
        <td align="center">
            2018/3/31
        </td>
        <td align="center">
            2018/3/31
        </td>
        <td align="center">
            是
        </td>
        <td align="center">
            無
        </td>
    </tr>
    <tr>
        <td align="center">
            <a href="./let_and_const_cmd">Let與Var的作用域</a>
        </td>
        <td align="center">
            區分Let與Var的作用域，與在區塊與流程上的差異。
        </td>
        <td align="center">
            2018/4/7
        </td>
        <td align="center">
            2018/4/7
        </td>
        <td align="center">
            是
        </td>
        <td align="center">
            無
        </td>
    </tr>
    <tr>
        <td align="center">
            <a href="./modetophp">NodeJS轉接網頁</a>
        </td>
        <td align="center">
            測試NodeJS是否能夠成功轉送資料。
        </td>
        <td align="center">
            2018/4/25
        </td>
        <td align="center">
            2018/4/25
        </td>
        <td align="center">
            是
        </td>
        <td align="center">
            無
        </td>
    </tr>
</table>