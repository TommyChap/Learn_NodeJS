console.log("let與var的作用域測試");
{
	let var_let = 1;
	var var_var = 2;
}
console.log("let變數內容為not defined");
console.log("var變數內容為" + var_var);

console.log("\n\n讓變數指向函數");
var a = [];
for (let i = 0; i < 10; i++){
	a[i] = function(){
		console.log(i);
	};
}
a[6]();

console.log("\n\n作用域認知與測試");
for (let i = 0; i < 3; i++){
	let i = 'abc';
	console.log(i);
}
